define([
    'jquery',
    'mage/cookies'
], function($){
    'use strict';
    return {
        set: function(name, value) {
            $.cookie(name, value);
        }
    };
});