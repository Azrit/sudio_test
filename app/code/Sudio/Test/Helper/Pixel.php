<?php

namespace Sudio\Test\Helper;

use Magento\Framework\Stdlib\CookieManagerInterface;

/**
 * Class Pixel
 * @package Sudio\Test\Helper
 */
class Pixel
{
    const COOKIE_NAME = 'pixel';

    /** @var CookieManagerInterface  */
    protected $cookieManager;

    /**
     * Pixel constructor.
     * @param CookieManagerInterface $cookieManager
     */
    public function __construct(
        CookieManagerInterface $cookieManager
    ) {
        $this->cookieManager = $cookieManager;
    }

    /**
     * @return null|string
     */
    public function getCustomerValue()
    {
        return $this->cookieManager->getCookie(self::COOKIE_NAME);
    }
}