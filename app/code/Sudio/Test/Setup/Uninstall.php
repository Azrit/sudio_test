<?php

namespace Sudio\Test\Setup;

use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Class Uninstall
 * @package Sudio\Test\Setup
 */
class Uninstall implements UninstallInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        //Drop custom column from quote table
        $setup->getConnection()->dropColumn(
            $setup->getTable(InstallSchema::QUOTE_TABLE_NAME),
            InstallSchema::PIXEL_COLUMN_NAME
        );

        //Drop custom column from sales_order table
        $setup->getConnection()->dropColumn(
            $setup->getTable(InstallSchema::QUOTE_TABLE_NAME),
            InstallSchema::PIXEL_COLUMN_NAME
        );

        $installer->endSetup();
    }
}