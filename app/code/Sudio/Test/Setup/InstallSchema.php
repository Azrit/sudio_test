<?php

namespace Sudio\Test\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 */
class InstallSchema implements InstallSchemaInterface
{
    const QUOTE_TABLE_NAME = 'quote';
    const SALES_TABLE_NAME = 'sales_order';
    const PIXEL_COLUMN_NAME = 'pixel';

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        //Add custom column to the quote table
        $installer->getConnection()
            ->addColumn(
                $setup->getTable(self::QUOTE_TABLE_NAME),
                self::PIXEL_COLUMN_NAME,
                [
                    'type'     => Table::TYPE_TEXT,
                    'length'   => '256',
                    'default'  => '',
                    'nullable' => false,
                    'comment'  => 'Custom field from Sudio_Test'
                ]
            );

        //Add custom column to the sales_order table
        $installer->getConnection()
            ->addColumn(
                $setup->getTable(self::SALES_TABLE_NAME),
                self::PIXEL_COLUMN_NAME,
                [
                    'type'     => Table::TYPE_TEXT,
                    'length'   => '256',
                    'default'  => '',
                    'nullable' => false,
                    'comment'  => 'Custom field from Sudio_Test'
                ]
            );
        $installer->endSetup();
    }
}