<?php

namespace Sudio\Test\Block;

use Magento\Framework\View\Element\Template;
use Sudio\Test\Helper\Pixel;

/**
 * Class AddCookieJs
 * @package Sudio\Test\Block
 */
class AddCookieJs extends Template
{
    /**
     * @return string
     */
    public function getCookieName()
    {
        return \Zend_Json::encode(Pixel::COOKIE_NAME);
    }
}