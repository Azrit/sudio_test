<?php

namespace Sudio\Test\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Sudio\Test\Helper\Pixel;
use Psr\Log\LoggerInterface;

/**
 * Class AddToOrder
 * @package Sudio\Test\Observer
 */
class AddToOrder implements ObserverInterface
{
    /** @var Pixel  */
    protected $pixelHelper;

    /** @var LoggerInterface  */
    protected $logger;

    public function __construct(
        Pixel $pixelHelper,
        LoggerInterface $logger
    ) {
        $this->pixelHelper = $pixelHelper;
        $this->logger = $logger;
    }

    /**
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        if ($pixel = $this->pixelHelper->getCustomerValue()) {
            try {
                /** @var \Magento\Sales\Model\Order $order */
                $order = $observer->getEvent()->getOrder();
                $order->setPixel($pixel);
            } catch (\Exception $e) {
                $this->logger->critical($e);
            }
        }
        return $this;
    }
}